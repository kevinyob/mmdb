import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule,NbButtonModule,NbMenuModule,NbMenuItem } from '@nebular/theme';
import { NbSidebarModule, NbSidebarService,NbInputModule,NbCardModule } from '@nebular/theme';
import { MenuShowcaseComponent } from './menu-showcase/menu-showcase.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';





@NgModule({
  declarations: [
    AppComponent,
    MenuShowcaseComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
 
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbSidebarModule,
    HttpClientModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbMenuModule.forRoot(),
    
  
    
  ],
  providers: [NbSidebarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
