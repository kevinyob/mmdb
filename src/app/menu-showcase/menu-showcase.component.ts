import { Component, OnInit } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'app-menu-showcase',
  template: `
            <header>
            <div class="container">
                <a routerLink="/" class="logo">MMDB </a>
                <nav>
                <ul>
                  <li><a routerLink="/home">Home</a>
                  <li><a routerLink="/about">About</a>
                  <li><a routerLink="/contact">Contact Us</a>
                </ul>
                </nav>
              </div>
              </header>`,
  styleUrls: ['./menu-showcase.component.scss']
})
export class MenuShowcaseComponent implements OnInit {
  public name="Kevin";

  constructor() { }

  ngOnInit() {
  }
greetUser()
{
  return "Hello "+this.name;
}
onClick(value)
{
  this.name=value;

}
}
